
with System.Storage_Elements; use System.Storage_Elements;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Streams.Stream_Io;

with zlib ; use zlib ;

with hex ; use hex ;

package body res.unpack is

   procedure Unpack( resarg : res.ResourceType ; filename : String := "output.txt") is
      data : constant Stream_Element_Array := Unpack( resarg );
      file : Ada.Streams.Stream_Io.File_Type ;
   begin
      Ada.Streams.Stream_Io.Create( file , Ada.Streams.Stream_Io.Out_File , filename );
      Ada.Streams.Stream_Io.Write( file , data );
      Ada.Streams.Stream_Io.Flush( file );
      Ada.Streams.Stream_Io.Close( file );
   end Unpack ;

   function Unpack( resarg : res.ResourceType ) return Stream_Element_Array is

      resval : Storage_Array(1..Storage_Offset(Length(resarg.Data)/2)) := hex.Value(To_String(resarg.Data)) ;
      resvalstr : Stream_Element_Array(1..Stream_Element_Offset(Length(resarg.Data)/2)) ;
                  for resvalstr'Address use resval'Address;

      uncompressed_data : Stream_Element_Array(1..Stream_Element_Offset(resarg.Size)) ;
      uncompressed_str : String(1..resarg.Size);
         for uncompressed_str'Address use uncompressed_data'Address;
      
      Decompressor : zlib.Filter_Type;
      Block_Size : constant := 4;

      --  This makes sure that the last block contains
      --  only Adler checksum data.
      P : Stream_Element_Offset := 0 ;
      O : Stream_Element_Offset;
      L : Stream_Element_Offset := Stream_Element_Offset(resarg.Size);
   begin
      Put_Line(To_String(resarg.filename));
      Inflate_Init (Decompressor);
      loop
         Translate
            (Decompressor,
               resvalstr 
               (P + 1 .. 
                   Stream_Element_Offset'Min (P + Block_Size, L)) ,
               P,
               Uncompressed_Data
                 (Total_Out (Decompressor) + 1 .. Uncompressed_Data'Last),
               O,
               No_Flush);
               if verbose
               then
               Ada.Text_IO.Put_Line
                  ("Total in : " & zlib.Count'Image (Total_In (Decompressor)) &
                     ", out : " & zlib.Count'Image (Total_Out (Decompressor)));
               end if ;
               exit when Total_Out (Decompressor) = L;
      end loop;
      --Put_Line(uncompressed_str);
      return uncompressed_data ;
   end Unpack ;

end res.unpack ;
