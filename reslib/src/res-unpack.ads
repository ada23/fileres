with Ada.Streams ; use Ada.Streams;

package res.unpack is
   verbose : boolean := false ;
   procedure Unpack( resarg : res.ResourceType ; filename : String := "output.txt") ;
   function Unpack( resarg : res.ResourceType ) return Stream_Element_Array ;
end res.unpack ;
