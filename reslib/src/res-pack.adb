with Ada.Directories; use Ada.Directories;
with Ada.Streams ; 
with Ada.Streams.Stream_Io ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with Ada.Characters.Handling ; use Ada.Characters.Handling;
with zlib ; use zlib;

with hex.dump ;

package body res.pack is

   function Sanitize( fn : String ) return String is
      result : Unbounded_String := Null_Unbounded_String ;
   begin
      for c in fn'Range
      loop 
         if c = fn'First
         then
            if Is_Letter(fn(c))
            then
               Append( result , To_Upper(fn(c)));
            else
               Append( result , "V" );
            end if ;
         else
            if Is_Alphanumeric( fn(c) )
            then
               Append( result , To_Upper(fn(c))) ;
            else
               Append( result , "_" ) ;
            end if ;
         end if ;
      end loop ;
      return To_String(result);
   end Sanitize ;

   function Create(  pkgname : String := "resources" ) return ResourceFileType is
      result : ResourceFileType ;
   begin
      result.file := new File_Type ;
      Create( result.file.all , Out_File , pkgname & ".ads" );
      Put_Line( result.file.all , "with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ; ");
      Put_Line( result.file.all , "with res ; use res ;");
      Put( result.file.all , "package ");
      Put( result.file.all , pkgname ) ;
      Put_Line( result.file.all , " is");
      result.pkgname := To_Unbounded_String(pkgname);
      return result ;
   end Create ;

   procedure Add( resfile : ResourceFileType ; name : String ) is
        use Ada.Streams;
        file   : Ada.Streams.Stream_IO.File_Type;
        stream : Ada.Streams.Stream_IO.Stream_Access;
        filesize : Ada.Directories.File_Size ;
        Compressor : zlib.Filter_Type;
        I : Stream_Element_Offset;
    begin
        filesize := Ada.Directories.Size( name );
     
        Ada.Streams.Stream_IO.Open
            (file,
            Ada.Streams.Stream_IO.In_File,
            name);
        stream := Ada.Streams.Stream_IO.Stream (file);
        declare
            buffer : Ada.Streams.Stream_Element_Array
                 (1 .. Ada.Streams.Stream_Element_Offset (filesize));
            bufferlen : Ada.Streams.Stream_Element_Offset;
        begin
            stream.Read (buffer, bufferlen);
            pragma Assert(Integer(bufferlen) = Integer(filesize) );
            Deflate_Init (Compressor);
            declare
                compressed_data : Ada.Streams.Stream_Element_Array
                    (1 .. Ada.Streams.Stream_Element_Offset (filesize));
                compressed_size : Stream_Element_Offset;
            begin
                Translate (Compressor, buffer, I, Compressed_Data, compressed_size, Finish);
                pragma Assert (I = Buffer'Last);
                Close (Compressor);

               Set_Output( resfile.file.all );
               New_Line ;
               Put_Line( Ascii.HT & Sanitize(Simple_Name(name)) & " : res.ResourceType := ( ");
               Put_Line( Ascii.HT & Ascii.HT & "To_Unbounded_String( " & '"' & Simple_Name(name) & '"' & ") , ");
               Put_Line( Ascii.HT & Ascii.HT & filesize'Image & " , ");
               Put_Line( Ascii.HT & Ascii.HT & "To_Unbounded_String (");
               Hex.dump.Dump
                    (compressed_data'Address,
                    Integer (bufferlen),
                    false ,
                    true ,
                    32 ,
                    resfile.file.all );
               
               Put_Line( Ascii.HT & Ascii.HT & '"' & '"' &  " )) ;") ;
               Set_Output( Standard_Output );
            end;
        end ;
        Ada.Streams.Stream_IO.Close (file);

    exception
      when others =>
         Ada.Text_Io.Put_Line("Exception");
         raise;
   end Add ;

   procedure Close( file : ResourceFileType ) is
   begin
      Put( file.file.all , "end ");
      Put( file.file.all , To_String(file.pkgname) );
      Put_Line( file.file.all , " ;");
      Close( file.file.all );
   end Close ;

end res.pack ;