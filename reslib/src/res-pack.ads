with Ada.Text_Io; use Ada.Text_Io;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package res.pack is

   type ResourceFileType is record
      pkgname : Unbounded_String := Null_Unbounded_String ;
      file : access File_Type ;
   end record ;

   function Create( pkgname : String := "resources" ) return ResourceFileType;
   procedure Add( resfile : ResourceFileType ; name : String );
   procedure Close( file : ResourceFileType );
end res.pack ;