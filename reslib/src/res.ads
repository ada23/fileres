with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded;
package res is
   type ResourceType is
   record
      filename : Unbounded_String ;
      Size : Natural ;
      Data : Unbounded_String ;
   end record ;
end res ;