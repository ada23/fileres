with Ada.Text_Io; use Ada.Text_Io;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded;
with Ada.Streams ;
with res.unpack ;
with demo_resources ;

procedure demo is
begin
   declare
      d : Ada.Streams.Stream_Element_Array := res.unpack.Unpack( demo_resources.RES_PACK_ADS );
      dstr : String( 1..d'Length ) ;
	for dstr'Address use d'Address ;
   begin
      Put_Line(dstr);
   end ;
   Put("Creating the file "); Put_Line( To_String(demo_resources.RES_PACK_ADB.filename) & ".out");
   res.unpack.Unpack( demo_resources.RES_PACK_ADB , 
                      To_String(demo_resources.RES_PACK_ADB.filename) & ".out" );
end demo ;
