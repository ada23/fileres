with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_Io; use Ada.Text_io;
with Ada.Directories; use Ada.Directories;
with Ada.Streams; use Ada.Streams;
with System.Storage_Elements; use System.Storage_Elements;

with hex;
with zlib; use zlib;

with res ;
with res.pack ;

procedure fileres is
    resfile : res.pack.ResourceFileType ;
begin
   if Argument_Count < 2
   then
      Put_Line("fileres pkgname file1 file2 ...");
      return ;
   end if ;

   resfile := res.pack.Create (Argument(1));
   for arg in 2..Argument_Count
   loop
      Put_Line( Argument(arg) );
      res.pack.Add( resfile , Argument(Arg));
   end loop ;
   res.pack.Close( resfile );

end fileres;